$(document).ready(function(){
  console.log("Welcome to the Game");

// Para que salga la pregunta1 al hacer click
  $("#play").click(function(){
     $("#idiomas").show();
   });
$("#play").click(function(){
  $("#play").hide();
  $("#play1").hide();
});

// BARRA DE PROGRESSO


 $("#bandera-ing").click(function(){
   $("#idiomas").hide();
   $("#pregunta1").show();
 });

 $("#bandera-it").click(function(){
   $("#idiomas").hide();
   $("#breve-introduccion").show();
 });

var percent = 0;
// INIICIO PREGUNTA1
$(".limpiar").click(function(){
  $("#input1").val('');
});

$("#input1")
  .keyup(function() {
    var value = $(this).val();
    $("#verification").click(function(){
      if (value == 'Esa bolsa no es tuya'){
        $(".correcion").show();
        $(".error").hide();
        percent +=7;
        $(".progress").css("width", percent+'%')
        $(".progress").attr('aria-valuenow', percent);
      }else{
        $(".error").show();
        $(".error").fadeOut(1000);
      }
});
});
$(".continuar1").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta1").hide(2500);
  $("#pregunta2").show(2501);
});

// FIN PREGUNTA 1

// INICICIO PREGUNTA 2
$(".limpiar").click(function(){
  $("#input2").val('');
});

$(".pruebas").click(function(){
  var textBoto = $(this).text();
  var textAntic = $("#input2").val();
  $("#input2").val(textAntic + textBoto);

  $("#verification2").click(function(){
  if ( textAntic + textBoto == ' ayer  puse  más  jabón  en  el  baño '){
    $(".correcion").show();
    $(".error2").hide();
    percent +=7;
    $(".progress").css("width", percent+'%')
    $(".progress").attr('aria-valuenow', percent);
  }else{
    $(".error2").show();
    $(".error2").fadeOut(1000);
  }
});
});
$(".continuar2").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta2").hide(2500);
  $("#pregunta3").show(2501);
});

// FIN PREGUNTA 2

// INICIO PREGUNTA 3
$(".seleccionable").click(function(){
//  $(this).css("border-color", "rgb(0, 204, 0)");
  $(this).siblings().each(function() {
    $(this).removeClass("seleccionado");
  });
  $(this).addClass("seleccionado");
});

$(".verification").click(function(){
  $(this).parent().find(".seleccionable").each(function() {
    // Si el seleccionable que estamos mirando tiene la clase selecvcionado y la clase c
    if ($(this).hasClass("seleccionado") && $(this).hasClass("c")) {
      $(".correcion").show();
      // $(".error3").hide();
      percent +=7;
      $(".progress").css("width", percent+'%')
      $(".progress").attr('aria-valuenow', percent);
    }else{
      $(".error3").show();
      $(".error3").fadeOut(1000);
    }
});
});
$(".continuar3").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta3").hide(2500);
  $("#pregunta4").show(2501);
});

// FIN PREGUNTA 3

// INICIO PREGUNTA 4
$(".limpiar").click(function(){
  $("#input4").val('');
});

$("#input4")
  .keyup(function() {
    var value = $(this).val();
    $("#verification4").click(function(){
      if (value == 'Why do we meet in a cinema?'){
        $(".correcion").show();
        $(".error4").hide();
        percent +=7;
        $(".progress").css("width", percent+'%')
        $(".progress").attr('aria-valuenow', percent);
      }else{
        $(".error4").show();
        $(".error4").fadeOut(1000);
      }
});
});
$(".continuar4").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta4").hide(2500);
  $("#pregunta5").show(2501);
});

// INICIO PREGUNTA 5
$(".seleccionable").click(function(){
//  $(this).css("border-color", "rgb(0, 204, 0)");
  $(this).siblings().each(function() {
    $(this).removeClass("seleccionado");
  });
  $(this).addClass("seleccionado");
});

$(".verification").click(function(){
  $(this).parent().find(".seleccionable").each(function() {
    // Si el seleccionable que estamos mirando tiene la clase selecvcionado y la clase c
    if ($(this).hasClass("seleccionado") && $(this).hasClass("c")) {
      $(".correcion").show();
      $(".error5").hide();
      percent +=7;
      $(".progress").css("width", percent+'%')
      $(".progress").attr('aria-valuenow', percent);
    }else{
      $(".error5").show();
      $(".error5").fadeOut(1000);
    }
});
});
$(".continuar5").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta5").hide(2500);
  $("#pregunta6").show(2501);
});
// FIN PREGUNTA 5

// INICIO PREGUNTA 6
$(".limpiar").click(function(){
  $("#input6").val('');
});

$(".pruebas1").click(function(){
  var textBoto = $(this).text();
  var textAntic = $("#input6").val();
  $("#input6").val(textAntic + textBoto);

  $("#verification6").click(function(){
  if ( textAntic + textBoto == ' me  quedé  en  casa  porque  tenía  un  resfriado '){
    $(".correcion").show();
    $(".error6").hide();
    percent +=7;
    $(".progress").css("width", percent+'%')
    $(".progress").attr('aria-valuenow', percent);
  }else{
    $(".error6").show();
    $(".error6").fadeOut(1000);
  }
});
});
$(".continuar6").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta6").hide(2500);
  $("#pregunta7").show(2501);
});
// FIN PREGUNTA 6

// INICIO PREGUNTA 7
$(".seleccionable").click(function(){
//  $(this).css("border-color", "rgb(0, 204, 0)");
  $(this).siblings().each(function() {
    $(this).removeClass("seleccionado");
  });
  $(this).addClass("seleccionado");
});

$(".verification7").click(function(){
  $(this).parent().find(".seleccionable").each(function() {
    // Si el seleccionable que estamos mirando tiene la clase selecvcionado y la clase c
    if ($(this).hasClass("seleccionado") && $(this).hasClass("c")) {
      $(".correcion").show();
      $(".error7").hide();
      percent +=7;
      $(".progress").css("width", percent+'%')
      $(".progress").attr('aria-valuenow', percent);
    }else{
      $(".error7").show();
      $(".error7").fadeOut(1000);
    }
});
});
$(".continuar7").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta7").hide(2500);
  $("#pregunta8").show(2501);
});
// FIN PREGUNTA 7

// INICIO PREGUNTA 8
$(".seleccionable").click(function(){
//  $(this).css("border-color", "rgb(0, 204, 0)");
  $(this).siblings().each(function() {
    $(this).removeClass("seleccionado");
  });
  $(this).addClass("seleccionado");
});

$(".verification8").click(function(){
  $(this).parent().find(".seleccionable").each(function() {
    // Si el seleccionable que estamos mirando tiene la clase selecvcionado y la clase c
    if ($(this).hasClass("seleccionado") && $(this).hasClass("c")) {
      $(".correcion").show();
      $(".error8").hide();
      percent +=7;
      $(".progress").css("width", percent+'%')
      $(".progress").attr('aria-valuenow', percent);
    }else{
      $(".error8").show();
      $(".error8").fadeOut(1000);
    }
});
});
$(".continuar8").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta8").hide(2500);
  $("#pregunta9").show(2501);
});
// FIN PREGUNTA 8

// INICIO PREGUNTA 9
$(".limpiar").click(function(){
  $("#input9").val('');
});

$(".pruebas2").click(function(){
  var textBoto = $(this).text();
  var textAntic = $("#input9").val();
  $("#input9").val(textAntic + textBoto);

  $("#verification9").click(function(){
  if ( textAntic + textBoto == ' Hacía  calor  y  nosotros  nadamons  todo  el  día '){
    $(".correcion").show();
    $(".error9").hide();
    percent +=7;
    $(".progress").css("width", percent+'%')
    $(".progress").attr('aria-valuenow', percent);
  }else{
    $(".error9").show();
    $(".error9").fadeOut(1000);
  }
});
});
$(".continuar9").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta9").hide(2500);
  $("#pregunta10").show(2501);
});
// FIN PREGUNTA 9

// INICIO PREGUNTA 10
$(".limpiar").click(function(){
  $("#input10").val('');
});

$("#input10")
  .keyup(function() {
    var value = $(this).val();
    $("#verification10").click(function(){
      if (value == 'The pink flowers are so pretty!'){
        $(".correcion").show();
        $(".error10").hide();
        percent +=8;
        $(".progress").css("width", percent+'%')
        $(".progress").attr('aria-valuenow', percent);
      }else{
        $(".error10").show();
        $(".error10").fadeOut(1000);
      }
});
});
$(".continuar10").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta10").hide(2500);
  $("#completado").show(2501);
});
// FIN PREGUNTA 10
$("#return").click(function(){
  $("#completado").hide(2000);
  percent = 0;
  $(".progress").css("width", "0px")
  $(".progress").attr('aria-valuenow', 0);
  $("#play").show(2500);
  $("#play1").show(2500);
});


// ITALIANO
// BREVE
$("#verification0it").click(function(){
  $("#breve-introduccion").hide();
  $("#pregunta1it").show();

});

// INICIO PREGUNTA 1
$(".seleccionable").click(function(){
//  $(this).css("border-color", "rgb(0, 204, 0)");
  $(this).siblings().each(function() {
    $(this).removeClass("seleccionado");
  });
  $(this).addClass("seleccionado");
});

$(".verification1it").click(function(){
  $(this).parent().find(".seleccionable").each(function() {
    // Si el seleccionable que estamos mirando tiene la clase selecvcionado y la clase c
    if ($(this).hasClass("seleccionado") && $(this).hasClass("c")) {
      $(".correcion").show();
      $(".error1it").hide();
      percent +=10;
      $(".progress").css("width", percent+'%')
      $(".progress").attr('aria-valuenow', percent);
    }else{
      $(".error1it").show();
      $(".error1it").fadeOut(1000);
    }
});
});
$(".continuar1it").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta1it").hide(2500);
  $("#pregunta2it").show(2501);
});
// FIN PREGUNTA 1

// INICIO PREGUNTA 2
$(".seleccionable").click(function(){
//  $(this).css("border-color", "rgb(0, 204, 0)");
  $(this).siblings().each(function() {
    $(this).removeClass("seleccionado");
  });
  $(this).addClass("seleccionado");
});

$(".verification2it").click(function(){
  $(this).parent().find(".seleccionable").each(function() {
    // Si el seleccionable que estamos mirando tiene la clase selecvcionado y la clase c
    if ($(this).hasClass("seleccionado") && $(this).hasClass("c")) {
      $(".correcion").show();
      $(".error2it").hide();
      percent +=10;
      $(".progress").css("width", percent+'%')
      $(".progress").attr('aria-valuenow', percent);
    }else{
      $(".error2it").show();
      $(".error2it").fadeOut(1000);
    }
});
});
$(".continuar2it").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta2it").hide(2500);
  $("#pregunta3it").show(2501);
});
// FIN PREGUNTA 2

// INICIO PREGUNTA 3
$(".limpiar").click(function(){
  $("#input3it").val('');
});

$(".pruebas3it").click(function(){
  var textBoto = $(this).text();
  var textAntic = $("#input3it").val();
  $("#input3it").val(textAntic + textBoto);

  $("#verification3it").click(function(){
  if ( textAntic + textBoto == ' Soy  una  mujer '){
    $(".correcion").show();
    $(".error3it").hide();
    percent +=10;
    $(".progress").css("width", percent+'%')
    $(".progress").attr('aria-valuenow', percent);
  }else{
    $(".error3it").show();
    $(".error3it").fadeOut(1000);
  }
});
});
$(".continuar3it").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta3it").hide(2500);
  $("#pregunta4it").show(2501);
});
// FIN PREGUNTA 3

// INICIO PREGUNTA 4
$(".limpiar").click(function(){
  $("#input4it").val('');
});

$(".pruebas4it").click(function(){
  var textBoto = $(this).text();
  var textAntic = $("#input4it").val();
  $("#input4it").val(textAntic + textBoto);

  $("#verification4it").click(function(){
  if ( textAntic + textBoto == ' Io  sono  la  ragazza '){
    $(".correcion").show();
    $(".error4it").hide();
    percent +=10;
    $(".progress").css("width", percent+'%')
    $(".progress").attr('aria-valuenow', percent);
  }else{
    $(".error4it").show();
    $(".error4it").fadeOut(1000);
  }
});
});
$(".continuar4it").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta4it").hide(2500);
  $("#pregunta5it").show(2501);
});
// FIN PREGUNTA 4

// INICIO PREGUNTA 5
$(".seleccionable").click(function(){
//  $(this).css("border-color", "rgb(0, 204, 0)");
  $(this).siblings().each(function() {
    $(this).removeClass("seleccionado");
  });
  $(this).addClass("seleccionado");
});

$(".verification5it").click(function(){
  $(this).parent().find(".seleccionable").each(function() {
    // Si el seleccionable que estamos mirando tiene la clase selecvcionado y la clase c
    if ($(this).hasClass("seleccionado") && $(this).hasClass("c")) {
      $(".correcion").show();
      $(".error5it").hide();
      percent +=10;
      $(".progress").css("width", percent+'%')
      $(".progress").attr('aria-valuenow', percent);
    }else{
      $(".error5it").show();
      $(".error5it").fadeOut(1000);
    }
});
});
$(".continuar5it").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta5it").hide(2500);
  $("#pregunta6it").show(2501);
});
// FIN PREGUNTA 5

// INCIO PREGUNTA 6
$(".seleccionable").click(function(){
//  $(this).css("border-color", "rgb(0, 204, 0)");
  $(this).siblings().each(function() {
    $(this).removeClass("seleccionado");
  });
  $(this).addClass("seleccionado");
});

$(".verification6it").click(function(){
  $(this).parent().find(".seleccionable").each(function() {
    // Si el seleccionable que estamos mirando tiene la clase selecvcionado y la clase c
    if ($(this).hasClass("seleccionado") && $(this).hasClass("c")) {
      $(".correcion").show();
      $(".error6it").hide();
      percent +=10;
      $(".progress").css("width", percent+'%')
      $(".progress").attr('aria-valuenow', percent);
    }else{
      $(".error6it").show();
      $(".error6it").fadeOut(1000);
    }
});
});
$(".continuar6it").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta6it").hide(2500);
  $("#pregunta7it").show(2501);
});
// FIN PREGUNTA 6

// INICIO PREGUNTA 7
$(".limpiar").click(function(){
  $("#input7it").val('');
});

$("#input7it")
  .keyup(function() {
    var value = $(this).val();
    $("#verification7it").click(function(){
      if (value == 'Yo bebo'){
        $(".correcion").show();
        $(".error7it").hide();
        percent +=10;
        $(".progress").css("width", percent+'%')
        $(".progress").attr('aria-valuenow', percent);
      }else{
        $(".error7it").show();
        $(".error7it").fadeOut(1000);
      }
});
});
$(".continuar7it").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta7it").hide(2500);
  $("#pregunta8it").show(2501);
});
// FIN PREGUNTA 7

// INICIO PREGUNTA 8
$(".limpiar").click(function(){
  $("#input8it").val('');
});

$(".pruebas8it").click(function(){
  var textBoto = $(this).text();
  var textAntic = $("#input8it").val();
  $("#input8it").val(textAntic + textBoto);

  $("#verification8it").click(function(){
  if ( textAntic + textBoto == ' tú  bevi  acqua '){
    $(".correcion").show();
    $(".error8it").hide();
    percent +=10;
    $(".progress").css("width", percent+'%')
    $(".progress").attr('aria-valuenow', percent);
  }else{
    $(".error8it").show();
    $(".error8it").fadeOut(1000);
  }
});
});
$(".continuar8it").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta8it").hide(2500);
  $("#pregunta9it").show(2501);
});
// FIN PREGUNTA 8

// INICIO PREGUNTA 9
$(".limpiar").click(function(){
  $("#input9it").val('');
});

$(".pruebas9it").click(function(){
  var textBoto = $(this).text();
  var textAntic = $("#input9it").val();
  $("#input9it").val(textAntic + textBoto);

  $("#verification9it").click(function(){
  if ( textAntic + textBoto == ' Buenos  días '){
    $(".correcion").show();
    $(".error9it").hide();
    percent +=6;
    $(".progress").css("width", percent+'%')
    $(".progress").attr('aria-valuenow', percent);
  }else{
    $(".error9it").show();
    $(".error9it").fadeOut(1000);
  }
});
});
$(".continuar9it").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta9it").hide(2500);
  $("#pregunta10it").show(2501);
});
// FIN PREGUNTA 9

// INICIO PREGUNTA 10
$(".limpiar").click(function(){
  $("#input10it").val('');
});

$("#input10it")
  .keyup(function() {
    var value = $(this).val();
    $("#verification10it").click(function(){
      if (value == 'No es una chica'){
        $(".correcion").show();
        $(".error10it").hide();
        percent +=4;
        $(".progress").css("width", percent+'%')
        $(".progress").attr('aria-valuenow', percent);
      }else{
        $(".error10it").show();
        $(".error10it").fadeOut(1000);
      }
});
});
$(".continuar10it").click(function(){
  $(".correcion").hide(2000);
  $("#pregunta10it").hide(2500);
  $("#completadoit").show(2501);
});
// FIN PREGUNTA 10
$("#returnit").click(function(){
  $("#completadoit").hide(2000);
  percent = 0;
  $(".progress").css("width", percent+'%')
  $(".progress").attr('aria-valuenow', percent);
  $("#play").show(2500);
  $("#play1").show(2500);
});



});
